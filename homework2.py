separator = '-' * 10 + '\nend task\n' + '-' * 10


def task1():
    name = input('Please, enter your name:')
    age = input('Please, enter your age:')
    print(f'Hello {name}! Your age is {age}')
    print(separator)


def task2():
    num = int(input('Please, enter a number:'))
    degree = num ** 132
    division = degree % 3
    print(f'Your {num} in 132 degree = {degree}, remainder of the division result on 3 = {division}')
    print(separator)


def task3():
    num1 = int(input('Enter first num:'))
    num2 = int(input('Enter second num:'))
    operations_dict = {
        '+': 'plus',
        '-': 'minus',
        '/': 'division',
        '//': 'integer division',
        '*': 'multiplying',
        '**': 'pow',
        '%': 'remainder of the division'
    }
    for keys in operations_dict.keys():
        print(f"{num1} {keys} {num2} = {eval(f'{num1} {keys} {num2}')}"
              f" this operation is called {operations_dict[keys]}")
    print(separator)


def task4():
    print('This function is counting "2a - 8b / (a-b+c)" formula.')
    a = int(input('Please, enter "a":'))
    b = int(input('Please, enter "b":'))
    c = int(input('Please, enter "c":'))
    # 5, 6, 1 = zero division
    try:
        print(2 * a - 8 * b / (a - b + c))
    except ZeroDivisionError:
        print('Error! Cant divide on zero!')
    print(separator)


def task5():
    string = input('Please, enter text:')
    num = int(input('Enter how much it will be repeated:'))
    print(string * num)
    print(separator)


def task6():
    num1 = 125
    num2 = 437
    divide_list = [2, 3, 10, 22]
    for num in divide_list:
        print(f'Remainder of the division {num1} on {num} = {num1 % num},'
              f' Remainder of the division {num2} on {num} = {num2 % num}')
    print(separator)


def task7():
    num1 = int(input('Enter first number:'))
    num2 = int(input('Enter second number'))
    print(int(num1 / num2))
    print(separator)


def task8():
    string1 = input('Enter first string:')
    string2 = input('Enter second string:')
    string3 = input('Enter third string:')
    print(f'{string1} {string2} {string3}')
    print(separator)


def task9():
    first = 15
    second = 43
    first = second
    second = first
    print(f"first: {first}, second: {second}")
    print(separator)


def task10():
    lst = [1, 6, 7, 8, 9]
    print(lst[len(lst)-1])
    print(separator)


def task11():
    lst = ['dog', 'fish', 3, 'pizza', 'car', 'people']
    num1 = int(input('Enter first number:'))
    num2 = int(input('Enter second number'))
    print(lst[num1:num2])
    print(separator)


def task12():
    string = input('Enter string. Program will return list of non repeating symbols from this string:')
    print(list(set(string)))
    print(separator)


def task13():
    set1 = {123, 45, 56, 1, 2}
    set2 = {6, 321, 1, 2, 3, 123}
    crossing = list(set1 & set2)
    print(crossing[0] - crossing[-1])
    print(separator)


def task14():
    list1 = [1, 3, 6, 8]
    list2 = [1, 5, 8, 7, 9]
    print(list(set(list1 + list2)))
    print(separator)


def task15():
    lst = [1, 6, 3, 4, 5]
    tpl = (1, 2, 7, 3, 9)
    print(list(set(lst) & set(tpl)))
    print(separator)


def task16():
    text = 'Today is sunny day and i am feeling fine'
    print(len(text.split()))
    print(separator)


for iter in range(1, 17):
    print(f'task{iter}')
    exec(f'task{iter}()')